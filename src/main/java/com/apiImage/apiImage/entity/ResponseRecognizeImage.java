package com.apiImage.apiImage.entity;

import org.springframework.web.multipart.MultipartFile;

public class ResponseRecognizeImage {

    private String dataImage;
    private Boolean status;
    private String pathImage;
    private String dumpImage;

    public ResponseRecognizeImage() {
    }

    public ResponseRecognizeImage(String dataImage, String pathImage) {
        this.dataImage = dataImage;
        this.pathImage = pathImage;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getPathImage() {
        return pathImage;
    }

    public void setPathImage(String pathImage) {
        this.pathImage = pathImage;
    }

    public String getDumpImage() {
        return dumpImage;
    }

    public void setDumpImage(String dumpImage) {
        this.dumpImage = dumpImage;
    }

    public String getDataImage() {
        return dataImage;
    }

    public void setDataImage(String dataImage) {
        this.dataImage = dataImage;
    }

    @Override
    public String toString() {
        return "ResponseRecognizeImage{" +
                "dataImage='" + dataImage + '\'' +
                ", status=" + status +
                ", pathImage='" + pathImage + '\'' +
                ", dumpImage='" + dumpImage + '\'' +
                '}';
    }
}
