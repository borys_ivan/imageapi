package com.apiImage.apiImage.recognizeImage;

import com.apiImage.apiImage.entity.ResponseRecognizeImage;
import org.apache.commons.io.FileUtils;
import org.bytedeco.javacpp.BytePointer;
import org.bytedeco.javacpp.lept;
import org.bytedeco.javacpp.tesseract;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.bytedeco.javacpp.lept.pixRead;


@Service
public class RecognizeImage {


    public ResponseRecognizeImage recognize(String dataImage, String pathDataImage) {

        ResponseRecognizeImage DataImage = new ResponseRecognizeImage();

        String path = new File("src/main/resources/image").getAbsolutePath();


        try {

            byte[] decodedBuffer = Base64.getMimeDecoder().decode(dataImage);
            FileUtils.writeByteArrayToFile(new File("src/main/resources/image/image.jpg"), decodedBuffer);

            Stream<Path> paths = Files.walk(Paths.get(path));
            List<Path> pathsImage = paths.filter(Files::isRegularFile).collect(Collectors.toList());

            BytePointer outText = new BytePointer();
            tesseract.TessBaseAPI api = new tesseract.TessBaseAPI();

            String dataPath = "src/tessdata";

            api.SetVariable("tessedit_char_whitelist", ".,0123456789%/АБВГҐДЕЄЖЗИІЇЙКЛМНОПРСТУФХЦЧШЩЬЮЯ");

            // Initialize tesseract-ocr with English, without specifying tessdata path
            if (api.Init(dataPath, "ukr") != 0) {
                System.err.println("Could not initialize tesseract.");
                System.exit(1);
            }

            String strImageName;
            String textOCR;
            String fixTextOCR;
            String removedOptionalSymb;


            for (Path pathImage : pathsImage) {

               /* byte[] fileContent = FileUtils.readFileToByteArray(new File(pathImage.toUri()));
                String encodedString = Base64.getEncoder().encodeToString(fileContent);

                System.out.println(encodedString);*/


                /*String encodedString = Base64.;
                byte[] decodedBytes = Base64.getDecoder().decode(dataImage.getDataImage());
                FileUtils.writeByteArrayToFile(new File("src/main/resources/image2/2.jpg"), decodedBytes);*/


                lept.PIX image = pixRead(pathImage.toString());

                api.SetImage(image);

                api.SetSourceResolution(600);

                // Get OCR result
                outText = api.GetUTF8Text();

                textOCR = "OCR output:\n" + outText.getString();

                fixTextOCR = Pattern.compile(
                        "OCR output\\:\\n(.*?(?:г\\.?\\n|кг\\.?\\n|шт\\.?\\n|мл\\.?\\n|л\\.?\\n|уп\\.?\\n)).*?$",
                        Pattern.DOTALL)
                        .matcher(textOCR).replaceAll("$1");

                removedOptionalSymb = Pattern.compile(
                        "\n",
                        Pattern.DOTALL)
                        .matcher(fixTextOCR).replaceAll(" ");


                DataImage.setDumpImage(removedOptionalSymb);
                DataImage.setPathImage(pathDataImage);
                DataImage.setDataImage(null);


                //removed files
                Files.delete(pathImage);
            }


            // Destroy used object and release memory
            api.End();
            outText.deallocate();


        } catch (IOException e) {
            e.printStackTrace();
        }


        return DataImage;

    }


}