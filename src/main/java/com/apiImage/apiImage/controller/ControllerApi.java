package com.apiImage.apiImage.controller;

import com.apiImage.apiImage.entity.Response;
import com.apiImage.apiImage.entity.ResponseRecognizeImage;
import com.apiImage.apiImage.recognizeImage.RecognizeImage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
public class ControllerApi {

     @Autowired
     private RecognizeImage recognizeImage;


    @RequestMapping(method = RequestMethod.POST,value = "/hello")
    public ResponseRecognizeImage greeting(
            @RequestParam("dateImage") String dataImage,
            @RequestParam("pathImage") String pathImage

    ) {
        Response status;

        return recognizeImage.recognize(dataImage,pathImage);
    }


}
